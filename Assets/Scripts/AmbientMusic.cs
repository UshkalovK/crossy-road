﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientMusic : MonoBehaviour
{

    void Start()
    {
        if(PlayerPrefs.GetInt("isMusicEnabled") == 1)
            GetComponent<AudioSource>().Play();
    }
}
