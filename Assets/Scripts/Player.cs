﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject restartButton;
    [SerializeField] private GameObject musicButton;
    [SerializeField] private GameObject Audio;
    [SerializeField] private TerrainGenerator terrainGenerator;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text highscoreText;
    [SerializeField] private Text coinNumber;
    [SerializeField] private Text StartText;
    private int coins;
	private float jumpPower = 0.4f;
	private float jumpDuration = 0.05f;
    private bool isStart = true;
    private bool isOnLog;
    private bool isFirstCube;
    public bool isDead = false;
    public int score;
    
    private void Start()
    {
        score = 0;
        coins = PlayerPrefs.GetInt("Coins");
        coinNumber.text = "" + coins;
        isFirstCube = true;
        SwipeController.SwipeEvent += OnSwipeIvent;
        isStart = true;
        transform.Rotate(0f, 180f, 0f);
    }

    private void Update()
    {
        if (transform.position.x < 0.9)
        {
            isFirstCube = true;
        }
        if ((int)Mathf.Round(transform.position.x) > score)
        {
            score = (int)Mathf.Round(transform.position.x) ;
        }
    }
    
    private void ScoreCalc()
    {
        scoreText.text = "Score: " + score;
        Debug.Log(score);
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.GetComponent<MovingObject>() != null)
        {
            if (collision.collider.GetComponent<MovingObject>().isLog)
            {
                gameObject.GetComponent<Transform>().SetParent(null);
                isOnLog = false;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.GetComponent<MovingObject>() != null)
        {
            if (collision.collider.GetComponent<MovingObject>().isLog)
            {
                if (collision.collider.GetComponent<MovingObject>().transform.position.z > 15f ||
                    collision.collider.GetComponent<MovingObject>().transform.position.z < -15f
                    ) Die();
            }
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponent<MovingObject>() != null)
        {
            if (collision.collider.GetComponent<MovingObject>().isLog)
            {
                gameObject.GetComponent<Transform>().SetParent(collision.collider.gameObject.transform);
                isOnLog = true;
            }
        }

        if (collision.collider.GetComponent<CoinCollector>() != null)
        {
           
            PlayerPrefs.SetInt("Coins", ++coins);
            if(PlayerPrefs.GetInt("isMusicEnabled") == 1)
                Audio.GetComponent<AudioSource>().Play();
            collision.gameObject.SetActive(false);
            coinNumber.text = "" + PlayerPrefs.GetInt("Coins");
        }
    }

    public void Die()
	{
		isDead = true;
        gameObject.SetActive(false);
        highscoreText.text = "Highscore: " + PlayerPrefs.GetInt("Highscore");;
        highscoreText.gameObject.SetActive(true);
        restartButton.SetActive(true);
        musicButton.SetActive(true);
        SwipeController.SwipeEvent -= OnSwipeIvent;
	}

    void OnSwipeIvent(SwipeController.SwipeType type)
    {
        Vector3 offset = Vector3.zero;
        Vector3 rotation = Vector3.zero;
        bool needGenerateTerrain = false;
        if (!isDead)
        {
            switch (type)
            {
                case SwipeController.SwipeType.UP: offset = new Vector3(1, 0, 0);
                    needGenerateTerrain = true;
                    ScoreCalc();
                    if (isStart)
                    {
                        StartText.gameObject.SetActive(false);
                        scoreText.gameObject.SetActive(true);
                        transform.DORotate(new Vector3(0,0,0), jumpDuration);
                        isStart = false;
                        return;
                    }
                    isFirstCube = false;
                    rotation = new Vector3(0,0,0);
                    break;
                case SwipeController.SwipeType.LEFT: offset = new Vector3(0, 0, 1);
                    rotation = new Vector3(0,-90,0);
                    break;
                case SwipeController.SwipeType.RIGHT: offset = new Vector3(0, 0, -1);
                    rotation = new Vector3(0,90,0);
                    break;
                case SwipeController.SwipeType.DOWN: 
                    if(!isFirstCube || isOnLog)
                    offset = new Vector3(-1, 0, 0);
                    rotation = new Vector3(0,180,0);
                    break;
                default : Debug.LogError("New swipe type?");
                    return;
            }
        }
        

        var newPosition = transform.position + offset;
        var raycastPoint = transform.position + offset + Vector3.up * 3;

        if (!Physics.Raycast(raycastPoint, Vector3.down, 5, 1 << 8) && !isStart)
        {
            if(PlayerPrefs.GetInt("isMusicEnabled") == 1)
                GetComponent<AudioSource>().Play();
            transform.DOJump(newPosition, jumpPower, 1, jumpDuration);
            transform.DORotate(rotation, jumpDuration);
            
            if(needGenerateTerrain)
                terrainGenerator.SpawnTerrain(false, transform.position);
            }
        
        }
    }
