﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasButton : MonoBehaviour
{
	[SerializeField] private GameObject Ambient;
	[SerializeField] private Sprite musicOn, musicOff;
	
	private void Start()
	{
		if(PlayerPrefs.GetInt("isMusicEnabled") != 1 && gameObject.name == "Music")
			{
				GetComponent<Image>().sprite = musicOff;
			}
	}

    public void RestartGame()
    {
		SceneManager.LoadScene(0);
    }
	public void Music()
	{
		if(PlayerPrefs.GetInt("isMusicEnabled") != 1)
			{
				PlayerPrefs.SetInt("isMusicEnabled", 1);
				Ambient.GetComponent<AudioSource>().Play();
				GetComponent<Image>().sprite = musicOn;
			}
		else
			{
				PlayerPrefs.SetInt("isMusicEnabled", 0);
				Ambient.GetComponent<AudioSource>().Stop();
				GetComponent<Image>().sprite = musicOff;
			}
	}
}
