﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaterCollision : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponent<Player>() != null)
        {
			if(PlayerPrefs.GetInt("Highscore") < collision.collider.GetComponent<Player>().score)
			{
        		PlayerPrefs.SetInt("Highscore", collision.collider.GetComponent<Player>().score);
			}
			if(PlayerPrefs.GetInt("isMusicEnabled") == 1)
                GetComponent<AudioSource>().Play();
			collision.collider.GetComponent<Player>().Die();
            
			
        }
    }
}
