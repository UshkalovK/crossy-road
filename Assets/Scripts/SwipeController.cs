﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class SwipeController : MonoBehaviour
{
    private Vector2 tapPoint, swipeDelta;

    public delegate void OnSwipeInput(SwipeType type);

    public static event OnSwipeInput SwipeEvent;



    public enum SwipeType
    {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    void Awake()
    {
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            tapPoint = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            swipeDelta = (Vector2) Input.mousePosition - tapPoint;
            if (Mathf.Abs(swipeDelta.x) > Mathf.Abs(swipeDelta.y))
            {
                if (swipeDelta.x < 0) SwipeEvent(SwipeType.LEFT);
                else if (swipeDelta.x > 0) SwipeEvent(SwipeType.RIGHT);
            }
            else
            {
                if (swipeDelta.y < 0) SwipeEvent(SwipeType.DOWN);
                else if (swipeDelta.y >= 0) SwipeEvent(SwipeType.UP);
            }
        }
    }
}
