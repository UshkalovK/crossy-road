﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
    
public class MovingObjectSpawner : MonoBehaviour
{
    [SerializeField] private float minSeparationTime;
    [SerializeField] private float maxSeparationTime;
	[SerializeField] private bool isRight;
	[SerializeField] private Transform pool_parent;
    [SerializeField] private int pool_count = 5;

	private float minSpeed;
	private float maxSpeed;

	private int current_pool_element_ID = 0;

    void Start()
    {
        StartCoroutine(SpawnObject());
    }

    private IEnumerator SpawnObject()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minSeparationTime, maxSeparationTime));
            GameObject obj = pool_parent.GetChild(current_pool_element_ID).gameObject;
            obj.SetActive(true);
            obj.transform.position = pool_parent.position;
            obj.transform.rotation = pool_parent.rotation;
            current_pool_element_ID++;
			if(obj.GetComponent<MovingObject>().isLog)
			{
				minSpeed = 18f;
				maxSpeed = 25f;
			} else
			{
				minSpeed = 5f;
				maxSpeed = 12f;
			}
            if (isRight)
            {
				
                obj.transform.DOMove(
                    new Vector3(obj.transform.position.x, obj.transform.position.y, (float) (obj.transform.position.z + 50)), Random.Range(minSpeed, maxSpeed));
            }
            else
            {
                obj.transform.DOMove(
                    new Vector3(obj.transform.position.x, obj.transform.position.y, (float) (obj.transform.position.z - 50)), Random.Range(minSpeed, maxSpeed));
            }
            
            if (current_pool_element_ID > pool_parent.childCount - 1) current_pool_element_ID = 0;
        }
    }

    }
